#!/usr/bin/env bash
set -e

SERVER_PROPERTIES_FILE="${PWD}/server.properties"

function setServerProperty {
  local property=$1
  local variable=$2
  local value=${!variable}
  if [[ -v "${variable}" ]]; then
    echo "${property}=${value}"
    sed -i "/^${property}=/ s/=.*/=${value}/" "${SERVER_PROPERTIES_FILE}"
  else
    local default_property="$(sed -n /^${property}=.*$/p ${SERVER_PROPERTIES_FILE})"
    printf "%-58s %s\n" "$default_property" "(using default)"
  fi
}

setServerProperty "allow-cheats" "ALLOW_CHEATS"
setServerProperty "default-player-permission-level" "DEFAULT_PLAYER_PERMISSION_LEVEL"
setServerProperty "difficulty" "DIFFICULTY"
setServerProperty "gamemode" "GAMEMODE"
setServerProperty "level-name" "LEVEL_NAME"
setServerProperty "level-seed" "LEVEL_SEED"
setServerProperty "level-type" "LEVEL_TYPE"
setServerProperty "max-players" "MAX_PLAYERS"
setServerProperty "max-threads" "MAX_THREADS"
setServerProperty "online-mode" "ONLINE_MODE"
setServerProperty "player-idle-timeout" "PLAYER_IDLE_TIMEOUT"
setServerProperty "server-name" "SERVER_NAME"
setServerProperty "server-port" "SERVER_PORT"
setServerProperty "server-portv6" "SERVER_PORTV6"
setServerProperty "texturepack-required" "TEXTUREPACK_REQUIRED"
setServerProperty "tick-distance" "TICK_DISTANCE"
setServerProperty "view-distance" "VIEW_DISTANCE"
setServerProperty "white-list" "WHITE_LIST"

./bedrock_server
